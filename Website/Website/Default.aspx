﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Website.Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Projektowanie oprogramowania</h2>
    <h3>Logowanie</h3>
    <asp:Panel id="NotLoggedInPanel" runat="server">
        <table>
        <tr>
            <td>Login:</td>
            <td><asp:TextBox ID="LoginTextBox" runat="server" /></td>
        </tr>
        <tr>
            <td>Hasło:</td>
            <td><asp:TextBox ID="PasswordTextBox" runat="server" TextMode="Password" /></td>
        </tr>
        </table>
        <asp:Button runat="server" ID="LoginButton" Text="Zaloguj" OnClick="LoginButton_OnClick"/>
    </asp:Panel>
    <asp:Panel id="LoggedInPanel" runat="server">
        <asp:Label Font-Bold="True" Height="30" id="WelcomeLabel" runat="server">Witaj!</asp:Label>
        <asp:Button ID="LogoutButton" runat="server" OnClick="LogoutButton_OnClick" Text="Wyloguj"/>
    </asp:Panel>
</asp:Content>
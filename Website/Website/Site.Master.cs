﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Entities;

namespace Website
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterLink.Visible = Logic.Logic.LoggedInUser == null;
            PortfolioRatingLink.Visible = Logic.Logic.LoggedInUser is Juror;
            ArtworkRatingLink.Visible = Logic.Logic.LoggedInUser is Juror;
            UserManagementLink.Visible = Logic.Logic.LoggedInUser is Administrator;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Website.Entities;

namespace Website
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Logic.Logic.SetDummyDatabase();

            var user = Logic.Logic.LoggedInUser;
            if (user != null)
            {
                NotLoggedInPanel.Visible = false;
                LoggedInPanel.Visible = true;
                WelcomeLabel.Text = $"Witaj, {user.Name} {user.Surname}<br />Zalogowano jako: {user.GetType().ToString().Split('.')[2]}";
            }
            else
            {
                NotLoggedInPanel.Visible = true;
                LoggedInPanel.Visible = false;
            }
                
        }

        protected void LoginButton_OnClick(object _sender, EventArgs _e)
        {
            var success = Logic.Logic.Login(LoginTextBox.Text, PasswordTextBox.Text);

            if (success)
                Response.Redirect(Request.RawUrl);
            else
                MessageBox.Show("Nieprawidłowy login lub hasło");
        }

        protected void LogoutButton_OnClick(object _sender, EventArgs _e)
        {
            Logic.Logic.Logout();
            Response.Redirect(Request.RawUrl);
        }
    }
}
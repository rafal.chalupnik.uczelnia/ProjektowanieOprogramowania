﻿namespace Website.Entities
{
    public abstract class User
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Surname { get; set; }

        public string Role => GetType().ToString().Split('.')[2];
    }
}
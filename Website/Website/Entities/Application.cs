﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Entities
{
    public class Application
    {
        /*AppId INT PRIMARY KEY,
    AppEdition INT NOT NULL,
	AppNote CLOB NOT NULL,
    AppPaymentProof BLOB NULL,
    AppBiography BLOB NULL,
    AppPortfolio BLOB NULL,*/
        public int AppId { get; set; }
        public byte[] AppNote { get; set; }
        //public
        public byte[] AppPaymentProof { get; set; }
        public byte[] AppBiography { get; set; }
        public byte[] AppPortfolio { get; set; }
        public Edition AppEdition { get; set; }
    }
}
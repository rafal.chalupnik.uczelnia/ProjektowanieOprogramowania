﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Entities
{
    public class EntryPhoto
    {
        /*EpEntry INT NOT NULL,
	    EpPhoto INT NOT NULL,
	    EpContent BLOB NOT NULL,
	
        */
        public int EpPhoto { get; set; }
        public Entry EpEntry { get; set; }
        public byte[] EpContent { get; set; }
    }
}
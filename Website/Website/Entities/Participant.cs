﻿using System;

namespace Website.Entities
{
    public class Participant : User
    {
        public Address Address { get; set; }
        public DateTime Birthdate { get; set; }
        public string Birthplace { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
    }
}
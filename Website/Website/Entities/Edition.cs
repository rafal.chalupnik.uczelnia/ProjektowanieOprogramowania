﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Entities
{
    public class Edition
    {
        /*EdiId INT PRIMARY KEY,
	EdiYear INT NOT NULL UNIQUE,
	EdiResultsDate DATE NOT NULL,
	EdiResultsAnnouncement DATE NOT NULL,
	EdiApplicationDeadline DATE NOT NULL*/

        public int EdiId { get; set; }
        public int EdiYear { get; set; }
        public DateTime EdiResultsDate { get; set; }
        public DateTime EdiResultsAnnouncement { get; set; }
        public DateTime EdiApplicationDeadline { get; set; }
    }
}
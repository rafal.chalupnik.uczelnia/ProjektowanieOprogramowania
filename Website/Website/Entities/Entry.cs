﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Entities
{
    public class Entry
    {
        public int EntId { get; set; }
        public DateTime EntDate { get; set; }
        public string EntTitle { get; set; }
        public int EntCreationYear { get; set; }
        public bool EntPaid { get; set; }
        public bool EntSecondStage { get; set; }
        public string EntMaterial { get; set; }
        public string EntTechnique { get; set; }
        public Participant EntParticipant { get; set; }
        public Application EntApplication { get; set; }
        public string EntCategory { get; set; }
        
      
    }
}
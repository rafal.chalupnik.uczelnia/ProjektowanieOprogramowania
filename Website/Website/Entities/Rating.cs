﻿namespace Website.Entities
{
    public class Rating
    {
        public int RatApplication { get; set; }

        public int RatEntry { get; set; }

        public string RatJuror { get; set; }

        public byte Rat1 { get; set; }

        public byte Rat2 { get; set; }

        public byte Rat3 { get; set; }

        public byte Rat4 { get; set; }
    }
}

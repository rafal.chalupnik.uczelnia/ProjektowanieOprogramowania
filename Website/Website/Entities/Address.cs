﻿namespace Website.Entities
{
    public class Address
    {
        public int AddId { get; set; }
        public string AddCity { get; set; }
        public Country AddCountry { get; set; }
        public int AddNumber { get; set; }
        public string AddPostalCode { get; set; }
        public string AddStreet { get; set; }

        public override bool Equals(object _object)
        {
            return _object != null
                && _object is Address address
                && AddCity == address.AddCity
                && AddNumber == address.AddNumber
                && AddPostalCode == address.AddPostalCode
                && AddStreet == address.AddStreet
                && AddCountry.Equals(address.AddCountry);
        }
    }
}
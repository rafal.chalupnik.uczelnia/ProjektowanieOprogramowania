﻿namespace Website.Entities
{
    public class Juror : User
    {
        public Country Country { get; set; }
        public string Job { get; set; }
    }
}
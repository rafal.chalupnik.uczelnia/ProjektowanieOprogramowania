﻿namespace Website.Entities
{
    public class Country
    {
        public int CouId { get; set; }
        public string CouName { get; set; }

        public override bool Equals(object _object)
        {
            return _object != null
                   && _object is Country country
                   && CouName == country.CouName;
        }
    }
}
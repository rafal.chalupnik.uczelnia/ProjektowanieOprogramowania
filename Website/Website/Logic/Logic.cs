﻿using System;
using System.Collections.Generic;
using System.Linq;
using Website.DataAccess;
using Website.Entities;

namespace Website.Logic
{
    public class Logic
    {
        public static User LoggedInUser { get; private set; }
        public static List<Entry> Artworks { get; set; } = new List<Entry>();
        public static List<EntryPhoto> ArtworksPhoto { get; set; } = new List<EntryPhoto>();
        public static Participant uczestnik { get; set; } = new Participant();
        public static Address adresUcz { get; set; } = new Address();
        public static Application zgloszenie { get; set; } = new Application();
      

        public static User UserToEdit { get; set; }

        public static void AddUser(User _user)
        {
            if (_user == null) return;

            if (_user is Administrator admin)
                AdministratorDao.Create(admin);
            else if (_user is Juror juror)
            {
                var country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(juror.Country));
                if (country != null)
                    juror.Country = country;
                else
                {
                    CountryDao.Create(juror.Country);
                    juror.Country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(juror.Country));
                }

                JurorDao.Create(juror);
            }
            else if (_user is Participant participant)
            {
                var address = AddressDao.GetAll().SingleOrDefault(_a => _a.Equals(participant.Address));
                if (address != null)
                    participant.Address = address;
                else
                {
                    var country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(participant.Address.AddCountry));
                    if (country != null)
                        participant.Address.AddCountry = country;
                    else
                    {
                        CountryDao.Create(participant.Address.AddCountry);
                        participant.Address.AddCountry = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(participant.Address.AddCountry));
                    }

                    AddressDao.Create(participant.Address);
                    participant.Address = AddressDao.GetAll().SingleOrDefault(_a => _a.Equals(participant.Address));
                }

                ParticipantDao.Create(participant);
            }
        }
        public static void DeleteUser(User _user)
        {
            switch (_user)
            {
                case null:
                    return;
                case Administrator admin:
                    AdministratorDao.Delete(admin);
                    break;
                case Juror juror:
                    JurorDao.Delete(juror);
                    break;
                case Participant participant:
                    ParticipantDao.Delete(participant);
                    break;
            }
        }
        public static void EditUser(User _user)
        {
            if (_user == null) return;

            if (_user is Administrator admin)
                AdministratorDao.Update(admin);
            else if (_user is Juror juror)
            {
                var country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(juror.Country));
                if (country != null)
                    juror.Country = country;
                else
                {
                    CountryDao.Create(juror.Country);
                    juror.Country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(juror.Country));
                }

                JurorDao.Update(juror);
            }
            else if (_user is Participant participant)
            {
                var address = AddressDao.GetAll().SingleOrDefault(_c => _c.Equals(participant.Address));
                if (address != null)
                    participant.Address = address;
                else
                {
                    var country = CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(participant.Address.AddCountry));
                    if (country != null)
                        participant.Address.AddCountry = country;
                    else
                    {
                        CountryDao.Create(participant.Address.AddCountry);
                        participant.Address.AddCountry =
                            CountryDao.GetAll().SingleOrDefault(_c => _c.Equals(participant.Address.AddCountry));
                    }

                    AddressDao.Create(participant.Address);
                }

                ParticipantDao.Update(participant);
            }
        }
        public static List<User> GetAllUsers()
        {
            var output = new List<User>();
            output.AddRange(AdministratorDao.GetAll());
            output.AddRange(JurorDao.GetAll());
            output.AddRange(ParticipantDao.GetAll());
            return output;
        }
        public static bool Login(string _login, string _password)
        {
            LoggedInUser = AdministratorDao
                .GetAll().SingleOrDefault(_admin => _admin.Login == _login && _admin.Password == _password);
            if (LoggedInUser != null)
                return true;

            LoggedInUser = JurorDao
                .GetAll().SingleOrDefault(_juror => _juror.Login == _login && _juror.Password == _password);
            if (LoggedInUser != null)
                return true;

            LoggedInUser = ParticipantDao
                .GetAll().SingleOrDefault(_participant =>
                    _participant.Login == _login && _participant.Password == _password);

            return LoggedInUser != null;
        }
        public static void Logout()
        {
            LoggedInUser = null;
        }
        public static void SetDummyDatabase()
        {
            try
            {
                //AdministratorDao.GetAll();
                ApplicationDao.GetAll();
            }
            catch (Exception)
            {
                DatabaseDao.CreateDatabase();
                DatabaseDao.FillWithDummyData();
            }
        }
        public static List<Country> GetAllCountry()
        {
            var output = new List<Country>();
            output.AddRange(CountryDao.GetAll());
         
            return output;
        }

        public static List<string> GetAllNation()
        {
            var output = new List<string>();
            output.AddRange(CountryDao.GetAllNationality());

            return output;
        }
        public static List<EntryPhoto> GetAllPhoto()
        {
            var output = new List<EntryPhoto>();
            output.AddRange(EntryPhotoDao.GetAll());

            return output;
        }
        public static List<Application> GetAllPrac()
        {
            var output = new List<Application>();
            output.AddRange(ApplicationDao.GetAll());

            return output;
        }
        public static List<Edition> GetAllEd()
        {
            var output = new List<Edition>();
            output.AddRange(EditionDao.GetAll());

            return output;
        }
        public static List<Address> GetAllAdress()
        {
            var output = new List<Address>();
            output.AddRange(AddressDao.GetAll());

            return output;
        }
      

        public static void Registry()
        {
            AddressDao.Create(adresUcz);
          
            ApplicationDao.Create(zgloszenie);
            ParticipantDao.Create(uczestnik);
            foreach ( Entry x in Artworks )
            {
                EntryDao.Create(x);
            }
            foreach (EntryPhoto x in ArtworksPhoto)
            {
                EntryPhotoDao.Create(x);
            }

        }
    }
}
﻿<%@ Page Title="Sign in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ania.aspx.cs" Inherits="Website.Ania" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
       
     <h2><%# Eval("EntMaterial") %>.</h2>
  
    <h3 style="font-style: normal; font-weight: bold; color: #333333">Information</h3>
    
    <table>
        <tr>
            <td>Name</td>
            <td><asp:TextBox runat="server" ID="nameId" /></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Name is required  " ControlToValidate="nameId" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="nameId" ErrorMessage="Invalid Name!" ForeColor="#000099" ValidationExpression="[A-ZĆŁŃŚŻŹ]{1,1}[a-ząćęłńóśżź]{2,19}"></asp:RegularExpressionValidator>
            </td>

        </tr>
        <tr>
            <td>Surname</td>
            <td><asp:TextBox runat="server" ID="surnameId"/></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Surname is required " ControlToValidate="surnameId" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="surnameId" ErrorMessage="Invalid Surname! " ForeColor="#000099" ValidationExpression="[A-ZĆŁŃŚŻŹ]{1,1}[-]{0,1}[a-ząćęłńóśżź]{2,29}"></asp:RegularExpressionValidator>
            </td>
        </tr>
      
        <tr>
            <td>Place of Birth</td>
            <td>
                <asp:TextBox runat="server" ID="placeOfBirth"/>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Place of birth is required" ControlToValidate="placeOfBirth" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
     <tr>
         <td>
             Date of Birth
         </td>
         <td>
                 <asp:TextBox runat="server" ID="dateOfBirth" TextMode="Date"/>
         </td>
         <td>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Date of birth is required" ControlToValidate="dateOfBirth" ForeColor="Red"></asp:RequiredFieldValidator>
         </td>
     </tr>
        <tr>
            <td>Login</td>
        
        <td>
                 <asp:TextBox runat="server" ID="Login"/>
         </td>
         <td>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Login is required" ControlToValidate="Login" ForeColor="Red"></asp:RequiredFieldValidator>
         </td>
            </tr>

         <tr>
            <td>Password</td>
        
        <td>
                 <asp:TextBox runat="server" ID="Password" TextMode="Password"/>
         </td>
         <td>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password is required " ControlToValidate="Password" ForeColor="Red"></asp:RequiredFieldValidator>
         </td>
            </tr>
        <tr>
            <td style="height: 24px">
                Confirm Password
            </td>
            <td style="height: 24px">
                 <asp:TextBox runat="server" ID="PassConf" TextMode="Password"/>
            </td>
            <td style="height: 24px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="PassConf" ErrorMessage="Confirm Password!" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password" ControlToValidate="PassConf" ErrorMessage=" Both password mst be the same!" ForeColor="#660033"></asp:CompareValidator>
            </td>
        </tr>

     </table>
   
    <h3>Adress</h3>
    <table>
        <tr>
            <td>Country</td>
            <td>
                 <asp:DropDownList ID="CountryList" runat="server">
               
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="CountryList" ErrorMessage="You must choose a country" ForeColor="Red" InitialValue="Select"></asp:RequiredFieldValidator>
            </td>
           
        </tr>
        <tr>
            <td>City</td>
            <td>
                <asp:TextBox runat="server" ID="City"/>
            </td>
       
     
         <td>
             Postcode
         </td>
         <td>
                 <asp:TextBox runat="server" ID="Postcode"/>
         </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="City" ErrorMessage="City is required! " ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="Postcode" ErrorMessage="Postcode is required! " ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="Postcode" ErrorMessage=" Invalid Postcode!" ForeColor="#000099" ValidationExpression="[0-9]{2}-[0-9]{3}"></asp:RegularExpressionValidator>
            </td>
           <tr>
         <td>
             Street
         </td>
         <td>
                 <asp:TextBox runat="server" ID="Address"/>
         </td>
               <td>
             Nr
         </td>
         <td>
                 <asp:TextBox runat="server" ID="Nr"/>
         </td>
               </tr>
            <tr>
         <td>
             Phone
         </td>
         <td>
                 <asp:TextBox runat="server" ID="Phone" TextMode="Phone"/>
         </td>
         <td>
             Mobile Phone
         </td>
         <td>
                 <asp:TextBox runat="server" ID="Mobile" TextMode="Phone"/>
         </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Filed phone can not be empty!" ControlToValidate="Phone" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="Phone" ErrorMessage=" Invalid Phone! " ForeColor="#000099" ValidationExpression="\d{3}-\d{3}-\d{3}"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="Mobile" ErrorMessage="Invalid Mobile Phone! " ForeColor="#000099" ValidationExpression="\d{3}-\d{3}-\d{3}"></asp:RegularExpressionValidator>
                </td>
                
     </tr>
        <tr>
            <td>
                E-mail
            </td>
            <td>
                   <asp:TextBox runat="server" ID="Email" TextMode="Email"/>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Email is required" ControlToValidate="Email" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
                <tr>
            <td>
                Website
            </td>
            <td>
                   <asp:TextBox runat="server" ID="Website"/>
            </td>
                    <td>
                        </td>
        </tr>
     </table>
    <h3>Biography</h3>
    <table>
        <tr>
            <td>
                Biography
            </td>
        </tr>
         <tr>
            <td style="color: #808080; font-style: italic">
               500 charcters max.
                
            </td>
             <td style="width: 254px">
                                  <asp:FileUpload runat="server" ID="Biography2" />
               
                </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>
                 <asp:Button ID="AddArtwork" runat="server" Text="Add Artwok"
                     onclick="CountOnClick"   />
            </td>
           
                             
        </tr>
       
    </table>

     <h3>Fee</h3>
    <table>
        <tr>
            <td style="width: 385px">Payment of the application fee to Associazione Culturale MoCA with the reason of “Prize 17.18” </td>
        </tr>
        
            <td>Attach a fee confirmation file</td>
        </tr>
        <tr>
            <td style="width: 254px">
                 <asp:FileUpload runat="server" ID="Attach" />
                </td>

        </tr>
    
    </table>
    <h3>Privacy</h3>

    <table style="width: 1016px">
        <tr>
            
            <td>
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" Font-Italic="False" Font-Size="Smaller" Width="974px">
                    <asp:ListItem>I declare that I have read and accepted all the rules of the Arte Laguna Prize Regulations published on the site www.artelagunaprize.com</asp:ListItem>
                    <asp:ListItem>I Autorize MoCa Association to use my information as the Italian Privacy Law 675/96 and D.Lgs. 196/2003, also for the purpose of inclusion in databases operated by The Organization.</asp:ListItem>
                </asp:CheckBoxList>
            </td>

      </tr>
        <tr>
            <td></td>
             <td>
                <asp:Button ID="Button6" runat="server" Text="Cancel" Width="79px" OnClientClick="window.open('Default.aspx', 'Default');" />
            </td>
            <td>
                <asp:Button ID="Button3" runat="server" Text="Subbmit" Width="79px" OnClick="SubbmitOnClick" />
            </td>
           
        </tr>
    </table>

      <table class="table" id="tabArtworks">
        <thead style="font-weight: bold">
            <tr>
                <td>Id</td>
                <td>Title</td>
                <td>Material</td>
                <td>Technique</td>
                <td>Yaer</td>
               
            </tr>
        </thead>
        <asp:Repeater ID="ArtworkView" runat="server">
            <ItemTemplate>
                <tr>
                     <td><%# Eval("EntId") %></td>
                    <td><%# Eval("EntTitle") %></td>
                    <td><%# Eval("EntMaterial") %></td>
                    <td><%# Eval("EntTechnique") %></td>
                    <td><%# Eval("EntCreationYear") %></td>
                                   </tr>
            </ItemTemplate>
        </asp:Repeater>
         
</table>

        
   
                <asp:Button ID="Button7" runat="server" Text="Refresh" onclick="Button2_Click" />
               
  
 
    </asp:Content>

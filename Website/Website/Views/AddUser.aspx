﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="Website.AddUser" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td>
                <h2>Dodawanie użytkownika</h2>
                <h3>Wprowadź dane nowego użytkownika</h3>
            </td>
            <td>
                <asp:Button runat="server" Text="Dodaj" OnClick="AddUser_Click"/>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>Login:</td>
            <td><asp:TextBox ID="LoginTextBox" runat="server"/></td>
        </tr>
        <tr>
            <td>Hasło:</td>
            <td><asp:TextBox ID="PasswdTextBox" runat="server" TextMode="Password"/></td>
        </tr>
        <tr>
            <td>Nazwisko:</td>
            <td><asp:TextBox ID="SurnameTextBox" runat="server"/></td>
        </tr>
        <tr>
            <td>Imię:</td>
            <td><asp:TextBox ID="NameTextBox" runat="server"/></td>
        </tr>
        <tr>
            <td>Rola:</td>
            <td>
                <asp:DropDownList ID="RoleDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RoleDropDownList_OnTextChanged">
                    <asp:ListItem Text="Administrator"/>
                    <asp:ListItem Text="Juror"/>
                    <asp:ListItem Text="Uczestnik" Selected="true"/>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Birthdate" runat="server">
            <td>Data urodzenia:</td>
            <td>
                <asp:Calendar ID="BirthdateCalendar" runat="server" SelectedDate="2017/11/30"></asp:Calendar>
            </td>
        </tr>
        <tr id="Birthplace" runat="server">
            <td>Miejsce urodzenia:</td>
            <td><asp:TextBox ID="BirthplaceTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Street" runat="server">
            <td>Ulica:</td>
            <td><asp:TextBox ID="StreetTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Number" runat="server">
            <td>Nr domu:</td>
            <td><asp:TextBox ID="NumberTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="PostalCode" runat="server">
            <td>Kod pocztowy:</td>
            <td><asp:TextBox ID="PostalCodeTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="City" runat="server">
            <td>Miejscowość:</td>
            <td><asp:TextBox ID="CityTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Country" runat="server">
            <td>Kraj:</td>
            <td><asp:TextBox ID="CountryTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Phone" runat="server">
            <td>Telefon:</td>
            <td><asp:TextBox ID="PhoneTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="CellPhone" runat="server">
            <td>Telefon komórkowy:</td>
            <td><asp:TextBox ID="CellPhoneTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Email" runat="server">
            <td>E-mail:</td>
            <td><asp:TextBox ID="EmailTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Website" runat="server">
            <td>Strona internetowa:</td>
            <td><asp:TextBox ID="WebsiteTextBox" runat="server" ></asp:TextBox></td>
        </tr>
        <tr id="Job" runat="server">
            <td>Zawód</td>
            <td><asp:TextBox ID="JobTextBox" runat="server" ></asp:TextBox></td>
        </tr>
    </table>
</asp:Content>

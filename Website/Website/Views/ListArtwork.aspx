﻿<%@ Page Title="List Artwork" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ListArtwork.aspx.cs" Inherits="Website.ListArtwork" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <table>
        <tr>
            <td style="width: 137px"><h4>Nr Artwork</h4></td>
            <td style="width: 171px"><h4>Title</h4></td>
        </tr>
        <tr>
            <td style="width: 137px; height: 15px;">Artwork 1</td>
            <td style="width: 171px; height: 15px;"><h5>Krzyk</h5></td>
        </tr>
        <tr>
            <td style="width: 137px">Artwork 2</td>
            <td style="width: 171px"><h5>Upadek Faetona</h5></td>
        </tr>
        <tr>
            <td style="width: 137px">Artwork 3</td>
            <td style="width: 171px"><h5>Lśnienie</h5></td>
        </tr>
        <tr>
            <td style="width: 137px">Artwork 4</td>
            <td style="width: 171px"><h5>dot</h5></td>
        </tr>
        <tr>
            <td>
                 <asp:Button ID="Button2" runat="server" Text="Back" OnClientClick="window.open('Artwork.aspx', 'Artwork');" Width="88px" />
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Next" OnClientClick="window.open('Fee.aspx', 'Fee');" Width="95px" />
            </td>
        </tr>
    </table>
   
</asp:Content>

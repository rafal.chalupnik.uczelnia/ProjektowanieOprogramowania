﻿<%@ Page Title="Contact" Language="C#" EnableEventValidation="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="Website.Rafal" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td>
                <h2>Zarządzanie kontami</h2>
                <h3>Wybierz konto, aby nim zarządzać, lub dodaj nowe.</h3>
            </td>
            <td>
                <asp:Button runat="server" Text="Dodaj użytkownika" OnClick="OnClick"/>
            </td>
        </tr>
    </table>
    <table class="table">
        <thead style="font-weight: bold">
            <tr>
                <td>Login</td>
                <td>Nazwisko</td>
                <td>Imię</td>
                <td>Rola</td>
                <td>Edytuj</td>
                <td>Usuń</td>
            </tr>
        </thead>
        <asp:Repeater ID="UsersView" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Login") %></td>
                    <td><%# Eval("Surname") %></td>
                    <td><%# Eval("Name") %></td>
                    <td><%# Eval("Role") %></td>
                    <td>
                        <asp:Button runat="server" Text="Edytuj" OnClick="EditUserOnClick" CommandName="EditUser" CommandArgument='<%#Eval("Login")%>' />
                    </td>
                    <td>
                        <asp:Button OnClientClick="return confirm('Czy na pewno chcesz usunąć użytkownika?');" 
                                    CommandArgument='<%#Eval("Login")%>' CommandName="DeleteUser"
                                    Text="Usuń" runat="server" onclick="DeleteUserOnClick" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.DataAccess;
using Website.Entities;

namespace Website
{
    public partial class Rafal : Page
    {
        public List<User> Users;

        private void MessageBox(string _message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{_message}\");", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Logic.Logic.LoggedInUser is Administrator))
                Response.Redirect("../Default.aspx");

            Users = Logic.Logic.GetAllUsers();
            Users.Remove(Users.Single(_user => _user.Login == Logic.Logic.LoggedInUser.Login));

            UsersView.DataSource = Users;
            UsersView.DataBind();
        }

        protected void DeleteUserOnClick(object sender, EventArgs e)
        {
            var button = sender as Button;
            Logic.Logic.DeleteUser(Users.SingleOrDefault(_user => _user.Login == button.CommandArgument));
            Response.Redirect(Request.RawUrl);
        }

        protected void EditUserOnClick(object sender, EventArgs e)
        {
            var button = sender as Button;
            Logic.Logic.UserToEdit = Users.SingleOrDefault(_user => _user.Login == button.CommandArgument);
            Response.Redirect("EditUser.aspx");
        }

        protected void OnClick(object _sender, EventArgs _e)
        {
            Response.Redirect("AddUser.aspx");
        }
    }
}
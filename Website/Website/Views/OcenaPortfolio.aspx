﻿<%@ Page Title="Ocenianie portfolio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OcenaPortfolio.aspx.cs" Inherits="Website.OcenaPortfolio" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Portfolio do ocenienia: <%: List.Count %></h3>
    <table class="table">
        <thead style="font-weight: bold">
            <tr>
                <td></td>
                <td>Id</td>
                <td>Biografia</td>
                <td>Notatka</td>
                <td>Portfolio</td>
                <td>Ocena</td>
            </tr>
        </thead>
        <asp:Repeater ID="Entries" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="OperationCheck"/>
                    </td>
                    <td><%# Eval("AppId") %></td>
                    <td><asp:LinkButton runat="server" OnClick="BioDownload_Click">Pobierz biografię</asp:LinkButton></td>
                    <td><asp:LinkButton runat="server" OnClick="NoteDownload_Click">Pobierz notatkę</asp:LinkButton></td>
                    <td><asp:LinkButton runat="server" OnClick="PortfolioDownload_Click">Pobierz portfolio</asp:LinkButton></td>
                    <td>
                        <button onclick="$('#title').text('<%# Eval("Id") %>'); $('#<%= EntryId.ClientID %>').val('<%# Eval("Id") %>')" type="button" data-target="#popup" data-toggle="modal" class="btn btn-primary btn-xs">Oceń</button>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <div id="popup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ocena portfolio</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField runat="server" ID="EntryId" />
                    <div class="form-group">
                        <label for="title">ID:</label>
                        <label class="form-control" id="title"></label>
                    </div>
                    <div class="form-group">
                        <label for="points1">Ocena końcowa portfolio:</label>
                        <asp:TextBox TextMode="Number" runat="server" min="0" max="100" step="1" ID="points1" CssClass="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                    <asp:Button runat="server" Text="Oceń" CssClass="btn btn-primary" OnClick="Score_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

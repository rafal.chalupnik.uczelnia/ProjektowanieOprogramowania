﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Entities;

namespace Website
{
    public partial class EditUser : Page
    {
        private void MessageBox(string _message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{_message}\");", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!(Logic.Logic.LoggedInUser is Administrator))
                Response.Redirect("../Default.aspx");
            if (IsPostBack) return;

            var user = Logic.Logic.UserToEdit;

            if (user == null)
                Response.Redirect("UserManagement.aspx");

            RoleTextBox.Text = user.GetType().ToString().Split('.')[2];

            if (user is Administrator admin)
            {
                Birthdate.Visible = false;
                Birthplace.Visible = false;
                Street.Visible = false;
                Number.Visible = false;
                PostalCode.Visible = false;
                City.Visible = false;
                Country.Visible = false;
                Phone.Visible = false;
                CellPhone.Visible = false;
                Email.Visible = false;
                Website.Visible = false;
                Job.Visible = false;

                LoginTextBox.Text = admin.Login;
                SurnameTextBox.Text = admin.Surname;
                NameTextBox.Text = admin.Name;
            }
            else if (user is Juror juror)
            {
                Birthdate.Visible = false;
                Birthplace.Visible = false;
                Street.Visible = false;
                Number.Visible = false;
                PostalCode.Visible = false;
                City.Visible = false;
                Country.Visible = true;
                Phone.Visible = false;
                CellPhone.Visible = false;
                Email.Visible = false;
                Website.Visible = false;
                Job.Visible = true;

                LoginTextBox.Text = juror.Login;
                SurnameTextBox.Text = juror.Surname;
                NameTextBox.Text = juror.Name;
                CountryTextBox.Text = juror.Country.CouName;
                JobTextBox.Text = juror.Job;
            }
            else if (user is Participant participant)
            {
                Birthdate.Visible = true;
                Birthplace.Visible = true;
                Street.Visible = true;
                Number.Visible = true;
                PostalCode.Visible = true;
                City.Visible = true;
                Country.Visible = true;
                Phone.Visible = true;
                CellPhone.Visible = true;
                Email.Visible = true;
                Website.Visible = true;
                Job.Visible = false;

                LoginTextBox.Text = participant.Login;
                SurnameTextBox.Text = participant.Surname;
                NameTextBox.Text = participant.Name;
                BirthdateCalendar.SelectedDate = participant.Birthdate;
                BirthplaceTextBox.Text = participant.Birthplace;
                StreetTextBox.Text = participant.Address.AddStreet;
                NumberTextBox.Text = participant.Address.AddNumber.ToString();
                PostalCodeTextBox.Text = participant.Address.AddPostalCode;
                CityTextBox.Text = participant.Address.AddCity;
                CountryTextBox.Text = participant.Address.AddCountry.CouName;
                PhoneTextBox.Text = participant.Phone;
                CellPhoneTextBox.Text = participant.CellPhone;
                EmailTextBox.Text = participant.Email;
                WebsiteTextBox.Text = participant.Website;
            }
        }

        protected void PasswordResetOnClick(object sender, EventArgs e)
        {
            MessageBox("Hasło zostało zresetowane.");
        }

        protected void OnClick(object _sender, EventArgs _e)
        {
            if (string.IsNullOrEmpty(SurnameTextBox.Text))
            {
                MessageBox("Pole Nazwisko nie może być puste");
                return;
            }
            if (string.IsNullOrEmpty(NameTextBox.Text))
            {
                MessageBox("Pole Nazwisko nie może być puste");
                return;
            }

            var user = Logic.Logic.UserToEdit;

            user.Surname = SurnameTextBox.Text;
            user.Name = NameTextBox.Text;

            if (!string.IsNullOrEmpty(PasswdTextBox.Text))
                user.Password = PasswdTextBox.Text;
            if (user is Administrator admin)
                Logic.Logic.EditUser(admin);
            else if (user is Juror juror)
            {
                if (string.IsNullOrEmpty(CountryTextBox.Text))
                {
                    MessageBox("Pole Kraj nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(JobTextBox.Text))
                {
                    MessageBox("Pole Zawód nie może być puste");
                    return;
                }
                juror.Job = JobTextBox.Text;
                juror.Country.CouName = CountryTextBox.Text;
                Logic.Logic.EditUser(juror);
            }
            else if (user is Participant participant)
            {
                if (string.IsNullOrEmpty(BirthplaceTextBox.Text))
                {
                    MessageBox("Pole Miejsce urodzenia nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(StreetTextBox.Text))
                {
                    MessageBox("Pole Ulica nie może być puste");
                    return;
                }
                int parsedNumber;
                if (string.IsNullOrEmpty(NumberTextBox.Text) || !int.TryParse(NumberTextBox.Text, out parsedNumber))
                {
                    MessageBox("Pole Numer nie może być puste lub jest nieprawidłowe");
                    return;
                }
                if (string.IsNullOrEmpty(PostalCodeTextBox.Text))
                {
                    MessageBox("Pole Kod pocztowy nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CityTextBox.Text))
                {
                    MessageBox("Pole Miasto nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CountryTextBox.Text))
                {
                    MessageBox("Pole Kraj nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(PhoneTextBox.Text))
                {
                    MessageBox("Pole Numer telefonu nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CellPhoneTextBox.Text))
                {
                    MessageBox("Pole Numer komórkowy nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(EmailTextBox.Text))
                {
                    MessageBox("Pole Email nie może być puste");
                    return;
                }

                participant.Birthdate = BirthdateCalendar.SelectedDate;
                participant.Birthplace = BirthplaceTextBox.Text;
                participant.Address.AddStreet = StreetTextBox.Text;
                participant.Address.AddNumber = parsedNumber;
                participant.Address.AddPostalCode = PostalCodeTextBox.Text;
                participant.Address.AddCity = CityTextBox.Text;
                participant.Address.AddCountry.CouName = CountryTextBox.Text;
                participant.Phone = PhoneTextBox.Text;
                participant.CellPhone = CellPhoneTextBox.Text;
                participant.Email = EmailTextBox.Text;
                participant.Website = WebsiteTextBox.Text;

                Logic.Logic.EditUser(participant);
            }
            
            Logic.Logic.UserToEdit = null;
            Response.Redirect("UserManagement.aspx");
        }
    }
}
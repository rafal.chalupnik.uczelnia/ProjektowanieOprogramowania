﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Entities;

namespace Website
{
    public partial class AddUser : Page
    {
        private const string Administrator = "Administrator";
        private const string Juror = "Juror";
        private const string Participant = "Uczestnik";

        private void MessageBox(string _message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{_message}\");", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Logic.Logic.LoggedInUser is Administrator))
                Response.Redirect("../Default.aspx");
        }

        protected void RoleDropDownList_OnTextChanged(object _sender, EventArgs _e)
        {
            var role = RoleDropDownList.SelectedValue;

            Birthdate.Visible = role == Participant;
            Birthplace.Visible = role == Participant;
            Street.Visible = role == Participant;
            Number.Visible = role == Participant;
            PostalCode.Visible = role == Participant;
            City.Visible = role == Participant;
            Country.Visible = role == Participant || role == Juror;
            Phone.Visible = role == Participant;
            CellPhone.Visible = role == Participant;
            Email.Visible = role == Participant;
            Website.Visible = role == Participant;
            Job.Visible = role == Juror;
        }

        protected void AddUser_Click(object _sender, EventArgs _e)
        {
            if (string.IsNullOrEmpty(LoginTextBox.Text))
            {
                MessageBox("Pole Login nie może być puste");
                return;
            }
            if (string.IsNullOrEmpty(PasswdTextBox.Text))
            {
                MessageBox("Pole Hasło nie może być puste");
                return;
            }
            if (string.IsNullOrEmpty(SurnameTextBox.Text))
            {
                MessageBox("Pole Nazwisko nie może być puste");
                return;
            }
            if (string.IsNullOrEmpty(NameTextBox.Text))
            {
                MessageBox("Pole Imię nie może być puste");
                return;
            }

            var role = RoleDropDownList.SelectedValue;

            if (role == Administrator)
            {
                Logic.Logic.AddUser(new Administrator()
                {
                    Login = LoginTextBox.Text,
                    Name = NameTextBox.Text,
                    Password = PasswdTextBox.Text,
                    Surname = SurnameTextBox.Text
                });
            }
            else if (role == Juror)
            {
                if (string.IsNullOrEmpty(JobTextBox.Text))
                {
                    MessageBox("Pole Zawód nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CountryTextBox.Text))
                {
                    MessageBox("Pole Kraj nie może być puste");
                    return;
                }

                Logic.Logic.AddUser(new Juror()
                {
                    Country = new Country() { CouName = CountryTextBox.Text },
                    Job = JobTextBox.Text,
                    Login = LoginTextBox.Text,
                    Name = NameTextBox.Text,
                    Password = PasswdTextBox.Text,
                    Surname = SurnameTextBox.Text
                });
            }
            else if (role == Participant)
            {
                if (string.IsNullOrEmpty(BirthplaceTextBox.Text))
                {
                    MessageBox("Pole Miejsce urodzenia nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(PhoneTextBox.Text))
                {
                    MessageBox("Pole Numer telefonu nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(EmailTextBox.Text))
                {
                    MessageBox("Pole Email nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(StreetTextBox.Text))
                {
                    MessageBox("Pole Ulica nie może być puste");
                    return;
                }
                int parsedNumber;
                if (string.IsNullOrEmpty(NumberTextBox.Text) || !int.TryParse(NumberTextBox.Text, out parsedNumber))
                {
                    MessageBox("Pole Numer nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(PostalCodeTextBox.Text))
                {
                    MessageBox("Pole Kod pocztowy nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CityTextBox.Text))
                {
                    MessageBox("Pole Miasto nie może być puste");
                    return;
                }
                if (string.IsNullOrEmpty(CountryTextBox.Text))
                {
                    MessageBox("Pole Kraj nie może być puste");
                    return;
                }

                Logic.Logic.AddUser(new Participant()
                {
                    Address = new Address()
                    {
                        AddCity = CityTextBox.Text,
                        AddCountry = new Country()
                        {
                            CouName = CountryTextBox.Text
                        },
                        AddNumber = parsedNumber,
                        AddPostalCode = PostalCodeTextBox.Text,
                        AddStreet = StreetTextBox.Text
                    },
                    Birthdate = BirthdateCalendar.SelectedDate,
                    Birthplace = BirthplaceTextBox.Text,
                    CellPhone = CellPhoneTextBox.Text,
                    Email = EmailTextBox.Text,
                    Login = LoginTextBox.Text,
                    Name = NameTextBox.Text,
                    Password = PasswdTextBox.Text,
                    Phone = PhoneTextBox.Text,
                    Surname = SurnameTextBox.Text,
                    Website = WebsiteTextBox.Text
                });
            }

            Response.Redirect("UserManagement.aspx");
        }
    }
}
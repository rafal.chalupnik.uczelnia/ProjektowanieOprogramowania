﻿<%@ Page  Title="Artwork" Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="Artwork.aspx.cs" Inherits="Website.Artwork" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <table>
        <tr>
            <td>Title</td>
            <td style="width: 254px"><asp:TextBox runat="server" Width="173px" ID="title"/></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="title" ErrorMessage="Title is required" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Year</td>
            <td style="width: 254px"><asp:TextBox runat="server" Width="174px" ID="Yaer"/></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Yaer" ErrorMessage="Yaer is required" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
       
        <tr>
            <td>Category</td>
            <td style="width: 254px">
                <asp:DropDownList ID="DropDownList1" runat="server">
                   
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownList1" ErrorMessage="Filed required" ForeColor="Red" InitialValue="Select"></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td>Material</td>
            <td style="width: 254px"><asp:TextBox runat="server" Width="173px" ID="Material"/></td>
             <td>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Material" ErrorMessage="Field material can not be empty!" ForeColor="Red"></asp:RequiredFieldValidator>
             </td>
        </tr>
         <tr>
            <td>Technique</td>
            <td style="width: 254px"><asp:TextBox runat="server" Width="174px" ID="technique"/></td>
             <td>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="technique" ErrorMessage="Field material can not be empty!" ForeColor="Red"></asp:RequiredFieldValidator>
             </td>
        </tr>
        <tr>
            <td>Attach Photos</td>
            <td style="width: 254px">
                 <asp:FileUpload runat="server" ID="Zdjecie" />
                </td>
            <td>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="You must Min 1 Photo!" ControlToValidate="Zdjecie" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            
        </tr>
        <tr>
             <td></td>
            <td style="width: 254px">
                 <asp:FileUpload runat="server" ID="Zdjecie2" />
                </td>

        </tr>
        <tr>
             <td></td>
            <td style="width: 254px">
                 <asp:FileUpload runat="server" ID="Zdjecie3" />
                </td>
        </tr>
        <tr>
            <td>
                 <asp:Button ID="Button9" runat="server" Text="Cancel" OnClientClick="window.close('Artwork.aspx', 'Artwork');"/>
            </td>
            <td style="width: 254px">
                <asp:Button ID="Next2" runat="server" Text="Save" onclick="SaveArtwork" />
               <asp:Button ID="Button1" runat="server" Text="Save" onclick="Nowy" />
            </td>
           
        </tr>
   
        </table>


     
 </asp:Content>
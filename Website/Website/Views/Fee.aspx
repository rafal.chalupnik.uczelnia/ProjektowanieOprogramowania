﻿<%@ Page Title="Attach"Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Fee.aspx.cs" Inherits="Website.Fee" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Fee</h3>
    <table>
        <tr>
            <td style="width: 385px">Payment of the application fee of:</td>
            <td>55$</td>
        </tr>
        <tr>
            <td style="width: 385px">Payed on (date)</td>
            <td><asp:TextBox runat="server" ID="surnameId"/></td>
        </tr>
        <tr>
            <td style="width: 385px">to Associazione Culturale MoCA with the reason of “Prize 17.18” through:</td>
        </tr>
        <tr>
            <td style="width: 385px">
                <asp:DropDownList ID="DropDownList1" runat="server" Height="41px" Width="459px">
                    <asp:ListItem>Bank Transfer</asp:ListItem>
                    <asp:ListItem>MoneyGram</asp:ListItem>
                    <asp:ListItem>Virtual POS</asp:ListItem>
                    <asp:ListItem>On-line with credit card</asp:ListItem>
                    <asp:ListItem Selected="True">Select...</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Attach a fee confirmation file</td>
        </tr>
        <tr>
            <td>
            <input id="File1" style="width: 448px; height: 20px" type="file" /></td></tr>
        
    </table>
    <h3>Privacy</h3>

    <table style="width: 1016px">
        <tr>
            
            <td>
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" Font-Italic="False" Font-Size="Smaller" Width="974px">
                    <asp:ListItem>I declare that I have read and accepted all the rules of the Arte Laguna Prize Regulations published on the site www.artelagunaprize.com</asp:ListItem>
                    <asp:ListItem>I Autorize MoCa Association to use my information as the Italian Privacy Law 675/96 and D.Lgs. 196/2003, also for the purpose of inclusion in databases operated by The Organization.</asp:ListItem>
                </asp:CheckBoxList>
            </td>

      </tr>
        <tr>
            <td></td>
             <td>
                <asp:Button ID="Button6" runat="server" Text="Cancel" Width="79px" OnClick="SubbmitOnClick" />
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Subbmit" Width="79px" OnClick="SubbmitOnClick" />
            </td>
           
        </tr>
    </table>
</asp:Content>

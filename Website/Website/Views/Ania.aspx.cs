﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Entities;

namespace Website
{
    public partial class Ania : Page
    {

        public List<string> con2=new List<string>();
        public List<Country> con = new List<Country>();
        public Participant user = new Participant();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                
                con2 = Logic.Logic.GetAllNation();
                con2.Insert(0, "Select");
                CountryList.DataSource = con2;

                CountryList.DataBind();

                if (Logic.Logic.uczestnik != null)
                {
                    nameId.Text = Logic.Logic.uczestnik.Name;

                    surnameId.Text = Logic.Logic.uczestnik.Surname;
                    dateOfBirth.Text = Logic.Logic.uczestnik.Birthdate.ToString();
                    placeOfBirth.Text = Logic.Logic.uczestnik.Birthplace;
                    Login.Text = Logic.Logic.uczestnik.Login;
                    Password.Text = Logic.Logic.uczestnik.Password;
                    PassConf.Text = Logic.Logic.uczestnik.Password;
                    Mobile.Text = Logic.Logic.uczestnik.CellPhone;
                    Email.Text = Logic.Logic.uczestnik.Email;
                    Website.Text = Logic.Logic.uczestnik.Website;
                    Phone.Text = Logic.Logic.uczestnik.Phone;
                }
                if (Logic.Logic.adresUcz != null)
                {
                   City.Text = Logic.Logic.adresUcz.AddCity;
                    Postcode.Text= Logic.Logic.adresUcz.AddPostalCode;
                    Address.Text= Logic.Logic.adresUcz.AddStreet;
                    Nr.Text = Logic.Logic.adresUcz.AddNumber.ToString();
                }
              
                
            }
           
            ArtworkView.DataSource = Logic.Logic.Artworks;
            ArtworkView.DataBind();
           



        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            ArtworkView.DataSource = Logic.Logic.Artworks;
            ArtworkView.DataBind();

        }

        private void MessageBox(string _message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{_message}\");", true);
        }

        protected void SubbmitOnClick(object sender, EventArgs e)
        {

            if (Logic.Logic.zgloszenie.AppPaymentProof==null)
            {
                string filename = Path.GetFileName(Attach.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
                string contentType = Attach.PostedFile.ContentType;
                using (Stream fs = Attach.PostedFile.InputStream)
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        if (bytes.Length > 0)
                        {

                            Logic.Logic.zgloszenie.AppPaymentProof = bytes;//?clob
                        }

                    }
                }
                if (Logic.Logic.zgloszenie.AppPaymentProof == null)
                {
                    MessageBox("Musisz dodać potwierdzenie opłaty");
                    return;
                }
            }
            if (Logic.Logic.zgloszenie.AppNote == null)
            {
                string filename = Path.GetFileName(Biography2.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
                string contentType = Biography2.PostedFile.ContentType;
                using (Stream fs = Biography2.PostedFile.InputStream)
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        if (bytes.Length > 0)
                        {

                            Logic.Logic.zgloszenie.AppNote = bytes;//?clob
                        }

                    }
                }
                if (Logic.Logic.zgloszenie.AppNote == null)
                {
                    MessageBox("Musisz dodać biografie");
                    return;
                }
                
            }
            if (Logic.Logic.Artworks.Count ==0)
            {
                MessageBox("Musisz dodać prace!");
                return;
            }

            var listPrac = Logic.Logic.GetAllPrac();
            var listEdycji = Logic.Logic.GetAllEd();
            var u = listEdycji.First();
            /*if (listPrac.First() == null)
                MessageBox("Musisz dodać prace!");*/
            var listaAdresów = Logic.Logic.GetAllAdress();
            Logic.Logic.adresUcz.AddId = listaAdresów.Count + 1;
            Logic.Logic.uczestnik.Address = Logic.Logic.adresUcz;



            Logic.Logic.zgloszenie.AppId=listPrac.Count+1;
            Logic.Logic.zgloszenie.AppEdition = u;//wyciągnac edycje
            foreach (Entry x in Logic.Logic.Artworks)
            {
                x.EntApplication = Logic.Logic.zgloszenie;
                x.EntParticipant = Logic.Logic.uczestnik;
            }
            Logic.Logic.Registry();
            MessageBox("Your registration was successful");
        }

        protected void CountOnClick(object sender, EventArgs e)
        {
            Logic.Logic.uczestnik.Name = nameId.Text;
            Logic.Logic.uczestnik.Surname = surnameId.Text;
            Logic.Logic.uczestnik.Birthdate = DateTime.Parse(dateOfBirth.Text);
            Logic.Logic.uczestnik.Birthplace = placeOfBirth.Text;
            Logic.Logic.uczestnik.Login = Login.Text;
            Logic.Logic.uczestnik.Password = Password.Text;
            Logic.Logic.uczestnik.CellPhone = Mobile.Text;
            Logic.Logic.uczestnik.Email = Email.Text;
            Logic.Logic.uczestnik.Website = Website.Text;
            Logic.Logic.uczestnik.Phone = Phone.Text ;


            
            Logic.Logic.adresUcz.AddCity = City.Text;
            Logic.Logic.adresUcz.AddNumber = Int32.Parse(Nr.Text);
            con = Logic.Logic.GetAllCountry();
            Logic.Logic.adresUcz.AddCountry = (con.Single(_c => _c.CouName == CountryList.SelectedValue));
          
            Logic.Logic.adresUcz.AddPostalCode = Postcode.Text;
            Logic.Logic.adresUcz.AddStreet = Address.Text;
            // Logic.Logic.zgloszenie.Note = Biography.Text;

            string filename = Path.GetFileName(Biography2.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
            string contentType = Biography2.PostedFile.ContentType;
            using (Stream fs = Biography2.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {

                        Logic.Logic.zgloszenie.AppNote = bytes;//?clob
                    }

                }
            }
            filename = Path.GetFileName(Attach.PostedFile.FileName);
            contentType = Attach.PostedFile.ContentType;
            using (Stream fs = Attach.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {
                        var listPhoto = Logic.Logic.GetAllPhoto();
                        
                        Logic.Logic.zgloszenie.AppPaymentProof = bytes;
                    }

                }
            }
            if (Logic.Logic.Artworks.Count==4)
            { MessageBox("You can't add more artwork"); }
            else
                Response.Redirect("Artwork.aspx");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Website.DataAccess;
using Website.Entities;

namespace Website
{
    public partial class OcenaPrac : Page
    {
        public List<Entry> List;

        protected void Page_Load(object sender, EventArgs e)
        {
            var ratings = RatingDao.GetAll();
            List = EntryDao.GetAll().Where(x => !ratings.Any(r => r.RatApplication == x.EntApplication.AppId && r.RatEntry == x.EntId)).ToList();

            Entries.DataSource = List;
            Entries.DataBind();
        }

        protected void Score_Click(object sender, EventArgs e)
        {
            Entry model = List.First(entry => entry.EntId.ToString() == EntryId.Value);
            if (byte.TryParse(points1.Text, out byte p1) && byte.TryParse(points1.Text, out byte p2) && byte.TryParse(points1.Text, out byte p3) && byte.TryParse(points1.Text, out byte p4))
            {
                RatingDao.Create(new Rating
                {
                    RatApplication = model.EntApplication.AppId,
                    RatEntry = model.EntId,
                    RatJuror = Logic.Logic.LoggedInUser.Login,
                    Rat1 = p1,
                    Rat2 = p2,
                    Rat3 = p3,
                    Rat4 = p4
                });
            }
        }
    }
}
﻿//------------------------------------------------------------------------------
// <generowany automatycznie>
//     Ten kod został wygenerowany przez narzędzie.
//
//     Modyfikacje tego pliku mogą spowodować niewłaściwe zachowanie i zostaną utracone
//     w przypadku ponownego wygenerowania kodu. 
// </generowany automatycznie>
//------------------------------------------------------------------------------

namespace Website {
    
    
    public partial class Artwork {
        
        /// <summary>
        /// Kontrolka title.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox title;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
        
        /// <summary>
        /// Kontrolka Yaer.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Yaer;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator2.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
        
        /// <summary>
        /// Kontrolka DropDownList1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DropDownList1;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator4.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
        
        /// <summary>
        /// Kontrolka Material.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Material;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator5.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
        
        /// <summary>
        /// Kontrolka technique.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox technique;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator6.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator6;
        
        /// <summary>
        /// Kontrolka Zdjecie.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload Zdjecie;
        
        /// <summary>
        /// Kontrolka RequiredFieldValidator7.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator7;
        
        /// <summary>
        /// Kontrolka Zdjecie2.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload Zdjecie2;
        
        /// <summary>
        /// Kontrolka Zdjecie3.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload Zdjecie3;
        
        /// <summary>
        /// Kontrolka Button9.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button9;
        
        /// <summary>
        /// Kontrolka Next2.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Next2;
        
        /// <summary>
        /// Kontrolka Button1.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.DataAccess;
using Website.Entities;

namespace Website
{
    public partial class OcenaPortfolio : Page
    {
        public List<Application> List;

        protected void Page_Load(object sender, EventArgs e)
        {
            var ratings = PortfolioRatingDao.GetAll();
            List = ApplicationDao.GetAll().Where(x => !ratings.Any(r => r.RatApplication == x.AppId)).ToList();

            Entries.DataSource = List;
            Entries.DataBind();
        }

        protected void Score_Click(object sender, EventArgs e)
        {
            Application model = List.First(entry => entry.AppId.ToString() == EntryId.Value);
            if (byte.TryParse(points1.Text, out byte p))
            {
                PortfolioRatingDao.Create(new PortfolioRating
                {
                    RatApplication = model.AppId,
                    RatEntry = -1,
                    RatJuror = Logic.Logic.LoggedInUser.Login,
                    Rat1 = p
                });
            }
        }

        protected void BioDownload_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            RepeaterItem item = (RepeaterItem)btn.NamingContainer;
            Application app = (Application)item.DataItem;
            DownloadBytes("biografia.txt", app.AppBiography);
        }

        protected void NoteDownload_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            RepeaterItem item = (RepeaterItem)btn.NamingContainer;
            Application app = (Application)item.DataItem;
            DownloadBytes("notatka.txt", app.Note);
        }

        protected void PortfolioDownload_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            RepeaterItem item = (RepeaterItem)btn.NamingContainer;
            Application app = (Application)item.DataItem;
            DownloadBytes("portfolio.zip", app.AppPortfolio);
        }

        private void DownloadBytes(string fileName, byte[] data)
        {
            try
            {
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.OutputStream.Write(data, 0, data.Length);
                Response.Flush();
            }
            catch (IOException) { }
        }
    }
}
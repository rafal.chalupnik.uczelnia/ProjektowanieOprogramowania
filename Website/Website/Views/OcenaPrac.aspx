﻿<%@ Page Title="Ocenianie prac" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OcenaPrac.aspx.cs" Inherits="Website.OcenaPrac" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Prac do ocenienia: <%: List.Count %></h3>
    <style>
        .artTitle {
            font-style: italic;
        }
    </style>
    <script>
        function updateImages(img1, img2, img3) {
            $('<%= Image1.ClientID %>').attr('src', 'data:image/png;base64,' + img1);
            $('<%= Image2.ClientID %>').attr('src', 'data:image/png;base64,' + img2);
            $('<%= Image3.ClientID %>').attr('src', 'data:image/png;base64,' + img3);
        }
    </script>
    <table class="table">
        <thead style="font-weight: bold">
            <tr>
                <td>Id</td>
                <td>Tytuł</td>
                <td>Rok stworzenia</td>
                <td>Data zgłoszenia</td>
                <td>Materiał</td>
                <td>Technika</td>
                <td>Podgląd</td>
                <td>Ocena</td>
            </tr>
        </thead>
        <asp:Repeater ID="Entries" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("EntId") %></td>
                    <td class="artTitle"><%# Eval("EntTitle") %></td>
                    <td><%# Eval("CreationYear") %></td>
                    <td><%# Eval("EntDate") %></td>
                    <td><%# Eval("EntMaterial") %></td>
                    <td><%# Eval("EntTechnique") %></td>
                    <td>
                        <button onclick="updateImages('<%# Eval("Img1") %>', '<%# Eval("Img2") %>', '<%# Eval("Img3") %>')" type="button" data-target="#imgPopup" data-toggle="modal" class="btn btn-info btn-xs">Pokaż</button>
                    </td>
                    <td>
                        <button onclick="$('#title').text('<%# Eval("Title") %>'); $('#<%= EntryId.ClientID %>').val('<%# Eval("Id") %>')" type="button" data-target="#popup" data-toggle="modal" class="btn btn-primary btn-xs">Oceń</button>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

    <div id="imgPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Podgląd pracy</h4>
                </div>
                <div class="modal-body">
                    <asp:Image runat="server" ID="Image1" calss="img-thumbnail" />
                    <asp:Image runat="server" ID="Image2" calss="img-thumbnail" />
                    <asp:Image runat="server" ID="Image3" calss="img-thumbnail" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>

    <div id="popup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ocena pracy</h4>
                </div>
                <div class="modal-body">
                    <asp:HiddenField runat="server" ID="EntryId" />
                    <div class="form-group">
                        <label for="title">Tytuł:</label>
                        <label class="form-control" id="title"></label>
                    </div>
                    <div class="form-group">
                        <label for="points1">Punkty kategorii 1:</label>
                        <asp:TextBox TextMode="Number" runat="server" min="0" max="25" step="1" ID="points1" CssClass="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="points2">Punkty kategorii 2:</label>
                        <asp:TextBox TextMode="Number" runat="server" min="0" max="25" step="1" ID="points2" CssClass="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="points3">Punkty kategorii 3:</label>
                        <asp:TextBox TextMode="Number" runat="server" min="0" max="25" step="1" ID="points3" CssClass="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="points4">Punkty kategorii 4:</label>
                        <asp:TextBox TextMode="Number" runat="server" min="0" max="25" step="1" ID="points4" CssClass="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                    <asp:Button runat="server" Text="Oceń" CssClass="btn btn-primary" OnClick="Score_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Website.Entities;

namespace Website
{
    public partial class Artwork : System.Web.UI.Page
    {
   
        public Entry Praca;
        public EntryPhoto Photo;
        public List<string> kategoria = new List<string> { "Painting", "SculptureAndInstallation", "PhotographicArt", "VideoArt", "Performance", "VirtualArt", "DigitalGraphic", "LandArt", "UrbanArt" };


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                kategoria.Insert(0, "Select");
                DropDownList1.DataSource = kategoria;

                DropDownList1.DataBind();
            }
        }
        private void MessageBox(string _message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", $"alert(\"{_message}\");", true);
        }
        public void SaveArtwork(object sender, EventArgs e)
        {
            //MessageBox("Your registration was successful");
           // Logic.Logic.adresUcz.AddCountry = DropDownList1.SelectedValue;

            Praca = new Entry() { EntId = Logic.Logic.Artworks.Count + 1, EntTitle = title.Text, EntTechnique = technique.Text, EntMaterial = Material.Text, EntDate = DateTime.Now, EntCreationYear = Int32.Parse(Yaer.Text), EntSecondStage=false, EntPaid=false, EntCategory= DropDownList1.SelectedValue };
            Logic.Logic.Artworks.Add(Praca);

            



            string filename = Path.GetFileName(Zdjecie.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
            string contentType = Zdjecie.PostedFile.ContentType;
            using (Stream fs = Zdjecie.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {
                        var listPhoto = Logic.Logic.GetAllPhoto();
                       
                        Photo = new EntryPhoto() { EpContent = bytes, EpPhoto = listPhoto.Count + 1, EpEntry = Praca };
                        Logic.Logic.ArtworksPhoto.Add(Photo);
                    }

                }
            }


             filename = Path.GetFileName(Zdjecie2.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
             contentType = Zdjecie2.PostedFile.ContentType;
            using (Stream fs = Zdjecie2.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {
                        var listPhoto = Logic.Logic.GetAllPhoto();

                        Photo = new EntryPhoto() { EpContent = bytes, EpPhoto = listPhoto.Count + 1, EpEntry = Praca };
                        Logic.Logic.ArtworksPhoto.Add(Photo);
                    }

                }
            }
            filename = Path.GetFileName(Zdjecie3.PostedFile.FileName);//jak nie ma pliku przypisuje byte[0]
            contentType = Zdjecie3.PostedFile.ContentType;
            using (Stream fs = Zdjecie3.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {
                        var listPhoto = Logic.Logic.GetAllPhoto();

                        Photo = new EntryPhoto() { EpContent = bytes, EpPhoto = listPhoto.Count + 1, EpEntry = Praca };
                        Logic.Logic.ArtworksPhoto.Add(Photo);
                    }

                }
            }
            Response.Redirect("Ania.aspx");
        }
        
        public void Nowy(object sender, EventArgs e)
        {
            //Logic.Logic.setEdition();
            Logic.Logic.SetDummyDatabase();
        }
        
       

    }
}
﻿using System.Collections.Generic;
using Website.Entities;

namespace Website.DataAccess
{
    public static class CountryDao
    {
        public static void Create(Country _country)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateCountry, _country);
        }

        public static void Delete(Country _country)
        {
            DatabaseDao.SqlExecute(SqlDictionary.DeleteCountry, _country);
        }

        public static IEnumerable<Country> GetAll()
        {
            return DatabaseDao.SqlQuery<Country>(SqlDictionary.GetAllCountries);
        }
        public static IEnumerable<string> GetAllNationality()//country??
        {
            return DatabaseDao.SqlQuery<string>(SqlDictionary.GetAllCouName);
        }

        public static void Update(Country _country)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateCountry, _country);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Web.Configuration;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public static class DatabaseDao
    {
        private static List<Administrator> administrators_ = new List<Administrator>()
        {
            new Administrator()
            {
                Login = "admin",
                Password = "admin",
                Name = "Jan",
                Surname = "Iksiński"
            }
        };

        private static List<Country> countries_ = new List<Country>()
        {
            new Country()
            {
                CouId = 1,
                CouName = "Polska"
            },
            new Country()
            {
                CouId = 2,
                CouName = "Niemcy"
            },
            new Country()
            {
                CouId = 3,
                CouName = "Luksemburg"
            }
        };

        private static List<Juror> jurors_ = new List<Juror>()
        {
            new Juror()
            {
                Login = "juror",
                Password = "juror",
                Name = "Henryk",
                Surname = "Igrekowski",
                Job = "Kustosz",
                Country = countries_[0]
            }
        };

        private static List<Address> addresses_ = new List<Address>()
        {
            new Address()
            {
                AddId = 1,
                AddCity = "Karmelkowo",
                AddCountry = countries_[0],
                AddNumber = 666,
                AddPostalCode = "12-345",
                AddStreet = "Karmelkowa"
            }
        };

        private static List<Participant> participants_ = new List<Participant>()
        {
            new Participant()
            {
                Login = "participant",
                Password = "participant",
                Name = "Ignacy",
                Surname = "Zetkowski",
                Address = addresses_[0],
                Birthdate = DateTime.Today,
                Birthplace = "Sosnowiec",
                CellPhone = "123456789",
                Email = "a@b.com",
                Phone = "987654321"
            }
        };

        public static void CreateDatabase()
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                connection.Execute(SqlDictionary.CreateDatabase);
            }
        }

        public static void FillWithDummyData()
        {
            foreach (var administrator in administrators_)
                AdministratorDao.Create(administrator);

            foreach (var country in countries_)
                CountryDao.Create(country);

            foreach (var juror in jurors_)
                JurorDao.Create(juror);

            foreach (var address in addresses_)
                AddressDao.Create(address);

            foreach (var participant in participants_)
                ParticipantDao.Create(participant);
            setEdition();
        }

        public static void SqlExecute(string _sql, object _params = null)
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                connection.Execute(_sql, _params);
            }
        }

        public static IEnumerable<T> SqlQuery<T>(string _sql, object _params = null)
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                return connection.Query<T>(_sql, _params);
            }
        }
        public static void setEdition()
        {

            Edition edition = new Edition();
            edition.EdiId = 1;
            edition.EdiYear = 2018;
            edition.EdiApplicationDeadline = new DateTime(2018, 2, 18);
            edition.EdiResultsAnnouncement = new DateTime(2018, 6, 20);
            edition.EdiResultsDate = new DateTime(2018, 6, 15);
            EditionDao.Create(edition);
        }
    }
}
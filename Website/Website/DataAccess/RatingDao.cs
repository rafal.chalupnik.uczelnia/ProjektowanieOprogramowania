﻿using System.Collections.Generic;
using Website.Entities;

namespace Website.DataAccess
{
    public static class RatingDao
    {
        public static void Create(Rating obj)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateRating, obj);
        }

        public static IEnumerable<Rating> GetAll()
        {
            return DatabaseDao.SqlQuery<Rating>(SqlDictionary.GetAllRatings);
        }

        public static void Update(Rating obj)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateRating, obj);
        }
    }
}

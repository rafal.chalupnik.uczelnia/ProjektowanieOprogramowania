﻿using System.Collections.Generic;
using Website.Entities;

namespace Website.DataAccess
{
    public static class AdministratorDao
    {
        public static void Create(Administrator _administrator)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateAdministrator, _administrator);
        }

        public static void Delete(Administrator _administrator)
        {
            DatabaseDao.SqlExecute(SqlDictionary.DeleteAdministrator, _administrator);
        }

        public static IEnumerable<Administrator> GetAll()
        {
            return DatabaseDao.SqlQuery<Administrator>(SqlDictionary.GetAllAdministrators);
        }

        public static void Update(Administrator _administrator)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateAdministrator, _administrator);
        }
    }
}
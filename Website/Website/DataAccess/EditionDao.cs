﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Website.Entities;

namespace Website.DataAccess
{
    public class EditionDao
    {
        public static void Create(Edition _edition)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateEdition, new
            {
                _edition.EdiId,
                _edition.EdiYear,
                _edition.EdiApplicationDeadline,
                _edition.EdiResultsAnnouncement,
                _edition.EdiResultsDate


            });
        }
        public static IEnumerable<Edition> GetAll()
        {
            return DatabaseDao.SqlQuery<Edition>(SqlDictionary.GetAllEdition);
        }

    }
}
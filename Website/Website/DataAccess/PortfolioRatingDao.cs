﻿using System.Collections.Generic;
using Website.Entities;

namespace Website.DataAccess
{
    public static class PortfolioRatingDao
    {
        public static void Create(PortfolioRating obj)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreatePortfolioRating, obj);
        }

        public static IEnumerable<PortfolioRating> GetAll()
        {
            return DatabaseDao.SqlQuery<PortfolioRating>(SqlDictionary.GetAllPortfolioRatings);
        }

        public static void Update(PortfolioRating obj)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdatePortfolioRating, obj);
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Configuration;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public static class AddressDao
    {
        public static void Create(Address _address)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateAddress, new
            {
                _address.AddCity,
                _address.AddNumber,
                _address.AddPostalCode,
                _address.AddStreet,
                AddCountry = _address.AddCountry.CouId
            });
        }

        public static void Delete(Address _address)
        {
            DatabaseDao.SqlExecute(SqlDictionary.DeleteAddress, _address);
        }

        public static IEnumerable<Address> GetAll()
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                return connection.Query<Address, Country, Address>(SqlDictionary.GetAllAddresses,
                    (_address, _country) =>
                    {
                        _address.AddCountry = _country;
                        return _address;
                    }, splitOn: "CouId");
            }
        }

        public static void Update(Address _address)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateAddress, new
            {
                _address.AddId,
                _address.AddCity,
                _address.AddNumber,
                _address.AddPostalCode,
                _address.AddStreet,
                AddCountry = _address.AddCountry.CouId
            });
        }
    }
}
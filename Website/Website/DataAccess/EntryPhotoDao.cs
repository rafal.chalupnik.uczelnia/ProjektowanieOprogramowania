﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Configuration;
using System.Web.WebSockets;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public class EntryPhotoDao
    {
        //dostosować do porzeb

        public static void Create(EntryPhoto _entryPhoto)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateEntryPhoto, new
            {
                _entryPhoto.EpContent,
                _entryPhoto.EpPhoto,

                EpEntry = _entryPhoto.EpEntry.EntId
            });
        }

    
        public static IEnumerable<EntryPhoto> GetAll()
        {
            return DatabaseDao.SqlQuery<EntryPhoto>(SqlDictionary.GetAllEntryPhoto);
        }

    }
}
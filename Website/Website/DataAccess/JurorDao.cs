﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Configuration;
using System.Web.WebSockets;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public static class JurorDao
    {
        public static void Create(Juror _juror)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateJuror, new
            {
                _juror.Login,
                _juror.Password,
                _juror.Name,
                _juror.Surname,
                _juror.Job,
                Country = _juror.Country.CouId
            });
        }

        public static void Delete(Juror _juror)
        {
            DatabaseDao.SqlExecute(SqlDictionary.DeleteJuror, _juror);
        }

        public static IEnumerable<Juror> GetAll()
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                return connection.Query<Juror, Country, Juror>(SqlDictionary.GetAllJurors,
                    (_juror, _country) =>
                    {
                        _juror.Country = _country;
                        return _juror;
                    }, splitOn: "CouId");
            }
        }

        public static void Update(Juror _juror)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateJuror, new
            {
                _juror.Login,
                _juror.Password,
                _juror.Name,
                _juror.Surname,
                _juror.Job,
                Country = _juror.Country.CouId
            });
        }
    }
}
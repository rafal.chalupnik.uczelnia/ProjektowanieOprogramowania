﻿using System.Collections.Generic;
using Website.Entities;

namespace Website.DataAccess
{
    public class EntryDao
    {
        public static void Create(Entry _entry)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateEntry, new
            {
                _entry.EntCreationYear,
                _entry.EntDate,
                _entry.EntId,
                _entry.EntMaterial,
                _entry.EntPaid,
                _entry.EntSecondStage,
                _entry.EntTechnique,
                _entry.EntTitle,
                _entry.EntCategory,


                EntParticipant = _entry.EntParticipant.Login,
                EntApplication = _entry.EntApplication.AppId
            });
        }

        public static IEnumerable<Entry> GetAll() =>
            DatabaseDao.SqlQuery<Entry>(SqlDictionary.GetAllEntries);
    }
}
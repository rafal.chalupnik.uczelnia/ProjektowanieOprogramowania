﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public class ApplicationDao
    {
        public static void Create(Application _application)
        {  
        DatabaseDao.SqlExecute(SqlDictionary.CreateAplication, new
            {
                _application.AppBiography,
                _application.AppId,
                _application.AppPortfolio,
                _application.AppNote,
                _application.AppPaymentProof,


            AppEdition = _application.AppEdition.EdiId
            });
        }


        public static IEnumerable<Application> GetAll()
        {
            return DatabaseDao.SqlQuery<Application>(SqlDictionary.GetAllApplication);
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Configuration;
using Dapper;
using Website.Entities;

namespace Website.DataAccess
{
    public static class ParticipantDao
    {
        public static void Create(Participant _participant)
        {
            DatabaseDao.SqlExecute(SqlDictionary.CreateParticipant, new
            {
                _participant.Birthdate,
                _participant.Birthplace,
                _participant.CellPhone,
                _participant.Email,
                _participant.Phone,
                _participant.Website,
                _participant.Login,
                _participant.Password,
                _participant.Name,
                _participant.Surname,
                Address = _participant.Address.AddId,
            });
        }

        public static void Delete(Participant _participant)
        {
            DatabaseDao.SqlExecute(SqlDictionary.DeleteParticipant, _participant);
        }

        public static IEnumerable<Participant> GetAll()
        {
            using (var connection = new SQLiteConnection(WebConfigurationManager.ConnectionStrings["SQLite"].ConnectionString))
            {
                return connection.Query<Participant, Address, Country, Participant>(SqlDictionary.GetAllParticipant,
                    (_participant, _address, _country) =>
                    {
                        _address.AddCountry = _country;
                        _participant.Address = _address;
                        return _participant;
                    }, splitOn: "AddId, CouId");
            }
        }

        public static void Update(Participant _participant)
        {
            DatabaseDao.SqlExecute(SqlDictionary.UpdateParticipant, new
            {
                _participant.Birthdate,
                _participant.Birthplace,
                _participant.CellPhone,
                _participant.Email,
                _participant.Phone,
                _participant.Website,
                _participant.Login,
                _participant.Password,
                _participant.Name,
                _participant.Surname,
                Address = _participant.Address.AddId,
            });
        }
    }
}